#!/bin/bash

count=10
server_address="127.0.0.1"
server_port=1900

for i in $(seq $count); do
    ./bin/Client_Type_C $server_address $server_port &
    ./bin/Client_Type_A /home/$USER/UNIX_ICP/UNIX_FILE && echo "$i" &
done

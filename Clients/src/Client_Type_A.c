#include "Common.h"
//#include <strings.h>

void VerificarArgumentosClientUNIX(int argc, char *argv[]);

int main(int argc, char *argv[])
{
    // Variables
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // Configurar socket
    int sockfd; // File Descriptro para el socket
    struct sockaddr_un servaddr; // Estructura para espesificar el server address

    // Enviar/Recibir
    long unsigned int sendBytes; // Cantidad de Bytes a enivar
    long int readBytes; // Cantidad de Bytes a enivar
    long int WriteReturnValue;

    // Obtener mensajes por STDIN para enviar/recibir al server
    char recvline[MAXLINE];   // Ingresado por stdin
    char string[MAXLINE];   // Ingresado por stdin
    char aux[MAXLINE];      // Sin /n
    char sendmsg[MAXLINE];
    char endOfMsg[7] = "\n";

    //unsigned int sleepTime;
    //--------------------------------------------------------------------------------------------------------------------------------------------

    VerificarArgumentosClientUNIX(argc, argv);

    // Crear Socket, AF_UNIX => Dentro de este sistema, SOCK_STRAM = Stream Socket, 0 = Protocolo por defecto (TCP)
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
    {
        printf("Falla al crear socket\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Handlear el address (path del archivo) del server con el que se quiere conectar
    //--------------------------------------------------------------------------------------------------------------------------------------------
    memset( (char *)&servaddr, '\0', sizeof(servaddr) );
	servaddr.sun_family = AF_UNIX;
    strcpy( servaddr.sun_path, argv[1]);
	//servlen = strlen( servaddr.sun_path) + sizeof(servaddr.sun_family);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Conectarse al servidor y handlear errores
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(connect (sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
    {
        printf("Falla al conectar con el servidor\n");
        exit(EXIT_FAILURE); 
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Recibir por argumentos elmensaje a enviar y el sleep time
    //--------------------------------------------------------------------------------------------------------------------------------------------
    strcpy(string,"SELECT * FROM Cars");
    //sleepTime = (unsigned int)atoi(argv[2]);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Construir el mensaje a enviar
    //--------------------------------------------------------------------------------------------------------------------------------------------
    string[strcspn(string,"\n")] = 0;      
    strcpy(aux,string);                     // Guardar string sin \n en aux evaular el while
    strcat(string,endOfMsg);                // Añadir fin de mensaje (se simula como el final de un mensaje HTTP)
    
    memset(sendmsg, 0 , MAXLINE); 
    strcat(sendmsg,"Tipo A | ");
    strcat(sendmsg,string);
    sendBytes = strlen(sendmsg); // Calcular la cantidad de caracteres del mensaje
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    //while(1)
    //{
        // sleep tiempo recibido
        //----------------------------------------------------------------------------------------------------------------------------------------
        //usleep(sleepTime);
        //----------------------------------------------------------------------------------------------------------------------------------------

        // Enviar el mensaje
        //----------------------------------------------------------------------------------------------------------------------------------------
        WriteReturnValue = write(sockfd, sendmsg, sendBytes);
        if((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
        {
            printf("Falla al enviar mensaje\n");
            exit(EXIT_FAILURE); 
        }
        //----------------------------------------------------------------------------------------------------------------------------------------

        // Recibir e imprimir respuesta
        //----------------------------------------------------------------------------------------------------------------------------------------
        memset(recvline, 0 , MAXLINE); 
        readBytes = 0;
        while((recvline[readBytes - 1] != '\n') && (recvline[readBytes - 2] != '\r')) // Detectar final de la querry
        {
            while(readBytes <= 0) // Esperar hasta que haya datos
            {
                readBytes = recv(sockfd, recvline, MAXLINE-1, MSG_DONTWAIT);
                if(readBytes > 0)
                {
                    // Aca se pueden guardar los datos en algun lado
                    //printf("Recibiendo %ld bytes\n",readBytes);
                }
            }
        }
        printf("Recibido: \n%s\n",recvline);
        //----------------------------------------------------------------------------------------------------------------------------------------
    //}
    
    //--------------------------------------------------------------------------------------------------------------------------------------------
    close(sockfd);      // Cerrar el socket
    exit(EXIT_SUCCESS); // Salir
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void VerificarArgumentosClientUNIX(int argc, char *argv[])
{
    // Verificar cantidad de argumentos
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(argc != 2)  
    {
        printf("cantidad de argumentos incorrecta\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que el nombre archivo para la conexion UNIX sea correcto
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((strlen(argv[1])>MAXLINE) || (!isValidFilename(argv[1])))
    {
        printf("Nombre de archivo incorrecto\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que la cantidad us a esperar antes de enviar de nuevo sea correcta
    //--------------------------------------------------------------------------------------------------------------------------------------------
    /*for(unsigned int i = 0; i < strlen(argv[2]); i ++)
    {
        if((isdigit(argv[2][i]) == 0)|| (atoi(argv[2]) <= 0)) 
        {                                                                
            printf("Debe ingresar una cantidad correcta\n");                
            exit(EXIT_FAILURE);
        }
    }*/                                           
    //--------------------------------------------------------------------------------------------------------------------------------------------
}
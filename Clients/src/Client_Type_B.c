#include "Common.h"

void VerificarArgumentosClientINET6(int argc, char *argv[]);

int main(int argc, char *argv[])
{
    // Variables
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // Configurar socket
    int sockfd; // File Descriptro para el socket
    struct sockaddr_in6 servaddr; // Estructura para espesificar el server address
    short unsigned int iport; // int para verificar num de puerto

    // Enviar/Recibir
    long unsigned int sendBytes; // Cantidad de Bytes a enivar
    long int readBytes; // Cantidad de Bytes a enivar
    long int WriteReturnValue;

    // Obtener mensajes por STDIN para enviar/recibir al server
    char recvline[MAXLINE];   // Ingresado por stdin
    char string[MAXLINE];   // Ingresado por stdin
    char aux[MAXLINE];      // Sin /n
    char sendmsg[MAXLINE];
    char endOfMsg[7] = "\n";
    char aux2[MAXLINE];      // Sin /n
    //--------------------------------------------------------------------------------------------------------------------------------------------

    VerificarArgumentosClientINET6(argc, argv);
    iport = (short unsigned int)atoi(argv[2]);

    // Crear Socket, AF_INET = Internet, SOCK_STRAM = Stream Socket, 0 = Protocolo por defecto (TCP)
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((sockfd = socket(AF_INET6, SOCK_STREAM, 0)) < 0)
    {
        printf("Falla al crear socket\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Handlear el address del server con el que se quiere conectar
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // bzero(void *s, size_t n) elimina los datos en los n bytes de memoria comenzando en la dirección donde apunta s, escribiendo cero.
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin6_family = AF_INET6;
    // htons (host to network, short) convierte cualquier tipo de orden de bytes al orden estandar de bytes de red. De esta forma, no importa
    // que formato de bytes tengan las aplicaciones que se conecten al socket, estas podrán comunicarse.
    servaddr.sin6_port = htons(iport);
    // int inet_pton(int af, const char *restrict src, void *restrict dst); Donde af = Address Family, convierte el string src en una estructura 
    // de direccion de red (ej: servaddr) con el formato de af y luego copia esa estructura en dst. De esta manera obtenemos la direccion a la
    // que queremos conectarnos mediante los argumentos.
    if(inet_pton(AF_INET6,argv[1],&servaddr.sin6_addr) <= 0)
    {
        printf("Falla al convertir dir IP a binario\n");
        exit(EXIT_FAILURE);
    }
    servaddr.sin6_scope_id = if_nametoindex(argv[3]); // enp2s0
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Conectarse al servidor y handlear errores
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(connect (sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
    {
        printf("Falla al conectar con el servidor\n");
        exit(EXIT_FAILURE); 
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Recibir por stdin los mensajes a enviar o detectar si se desea salir del programa
    //--------------------------------------------------------------------------------------------------------------------------------------------
    while(strcmp(aux,"salir") != 0)
    {
        // Construir el mensaje a enviar
        //----------------------------------------------------------------------------------------------------------------------------------------
        memset(string, 0 , MAXLINE); 
        memset(sendmsg, 0 , MAXLINE); 
        strcat(sendmsg,"Tipo B | ");
        printf("Ingrese el mensaje a enviar: ");
        //fgets(string,MAXLINE,stdin);            // Obtener string ingresado por stdin
        while(safeGetString(string, MAXLINE) == -1)
        {
            printf("Max cantidad de caracteres excedida (%d), ingrese un mensaje con tamaño correcto: ", MAXLINE - 2);
        }
        //strcat(string,"SELECT Name FROM Cars");
        string[strcspn(string,"\n")] = 0;       // Quitar \n para poder evaluar si se ha ingresado "salir"
        /*if(!strcmp(string,"salir"))
        {
            printf("detectado, salir\n");
            //break;
        }*/
        strcpy(aux,string);
        strcpy(aux2,string);                       // Guardar string sin \n en aux evaular el while
        strcat(string,endOfMsg);                // Añadir fin de mensaje (se simula como el final de un mensaje HTTP)
        //printf("El mensaje queda %sAAA\n",string); // Imprimir el mensaje final. Las AAA son para ver si hay saltos de linea en string

        // Enviar el mensaje
        //----------------------------------------------------------------------------------------------------------------------------------------
        // Enviar el mensaje:
        // ssize_t write(int fd, const void *buf, size_t count); Escribe hasta "count" Bytes del buffer comenzando en *buf en el archivo referido
        // por el file descriptor fd. Devuelve la cantidad de Bytes escritos en caso de exito o -1 en caso de error. Tambien puede devolver una 
        // cantidad de Bytes menor a count por alguna interrupcion u algun otro motivo, en cuyo caso tambien se handlea como un error
        strcat(sendmsg,string);
        sendBytes = strlen(sendmsg); // Calcular la cantidad de caracteres del mensaje
        WriteReturnValue = write(sockfd, sendmsg, sendBytes);
        if((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
        {
            printf("Falla al enviar mensaje\n");
            exit(EXIT_FAILURE); 
        }
        //----------------------------------------------------------------------------------------------------------------------------------------

        if(!strcmp(aux2,"salir"))
        {
            printf("detectado, salir\n");
            break;
        }

        // Recibir e imprimir respuesta
        //----------------------------------------------------------------------------------------------------------------------------------------
        memset(recvline, 0 , MAXLINE); 
        readBytes = 0;
        while((recvline[readBytes - 1] != '\n') && (recvline[readBytes - 2] != '\r')) // Detectar final de la querry
        {
            while(readBytes <= 0) // Esperar hasta que haya datos
            {
                readBytes = recv(sockfd, recvline, MAXLINE-1, MSG_DONTWAIT);
                if(readBytes > 0)
                {
                    // Aca se pueden guardar los datos en algun lado
                    //printf("Recibiendo %ld bytes\n",readBytes);
                }
            }
        }
        printf("Recibido: \n%s\n",recvline);
        //----------------------------------------------------------------------------------------------------------------------------------------

        // Verificar errores de lectura
        //----------------------------------------------------------------------------------------------------------------------------------------
        if(readBytes < 0)
        {
            printf("Error de lectura\n");
            exit(EXIT_FAILURE);
        }
        //----------------------------------------------------------------------------------------------------------------------------------------
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    //--------------------------------------------------------------------------------------------------------------------------------------------
    close(sockfd);      // Cerrar el socket
    exit(EXIT_SUCCESS); // Salir
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void VerificarArgumentosClientINET6(int argc, char *argv[])
{
    // Verificar cantidad de argumentos
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(argc != 4)  
    {
        printf("cantidad de argumentos incorrecta\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que el puerto ingresado en los argumentos sea correcto
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int i = 0; i < strlen(argv[2]); i ++)
    {
        if((isdigit(argv[2][i]) == 0)|| (atoi(argv[2]) <= 0) || (atoi(argv[2]) > 65535)) // Verificar que en el argumento para el puerto se haya
        {                                                                // ingresado un num y no letras ni caracteres especiales
            printf("Debe ingresar un puerto correcto\n");                // y que este num sea > = 0 y < 65535
            exit(EXIT_FAILURE);
        }
    }                                           // El valor se escribe en iport LUEGO de verrificar que el valor sea correcto ya el valor ingresado 
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que la interfaz ingresada solo consiste en letras y digitos. Si pasa esta prueba, de todas formas tiene que ser una interfaz
    // correcta, de otra manera va a fallar connect()
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int i = 0; i < strlen(argv[3]); i ++)
    {
        if(((isdigit(argv[3][i]) == 0) && (isalpha(argv[3][i]) == 0)) || strlen(argv[3]) > MAXLINE) 
        {                                                               
            printf("Debe ingresar una interfaz correcta\n");               
            exit(EXIT_FAILURE);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    
}
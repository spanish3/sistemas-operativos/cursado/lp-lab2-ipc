#include "Common.h"

#define FILEPATH "test.db"

void descargarArchivo(int *socket,unsigned long int file_size);
void VerificarArgumentosClientINET(int argc, char *argv[]);
void querryDePrueba();

int main(int argc, char *argv[])
{
    // Variables
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // Configurar socket
    int sockfd; // File Descriptro para el socket
    struct sockaddr_in servaddr; // Estructura para espesificar el server address
    short unsigned int iport; // int para verificar num de puerto

    // Enviar/Recibir
    long unsigned int sendBytes; // Cantidad de Bytes a enivar
    long int readBytes; // Cantidad de Bytes a enivar
    long int WriteReturnValue; // Varaible para controlar el return value de writer()

    // Obtener mensajes por STDIN para enviar/recibir al server
    char recvline[MAXLINE];  // Buffer para recibir
    char sendmsg[MAXLINE];   // Buffer para enviar
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar argumentos y obtener de estos el num de puerto
    //--------------------------------------------------------------------------------------------------------------------------------------------
    VerificarArgumentosClientINET(argc,argv);
    iport = (short unsigned int)atoi(argv[2]); 
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Crear Socket, AF_INET = Internet, SOCK_STRAM = Stream Socket, 0 = Protocolo por defecto (TCP)
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("Falla al crear socket\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Confiugrar address, familia y puerto del server con el que se quiere conectar
    //--------------------------------------------------------------------------------------------------------------------------------------------
    // bzero(void *s, size_t n) elimina los datos en los n bytes de memoria comenzando en la dirección donde apunta s, escribiendo cero.
    bzero(&servaddr, sizeof(servaddr)); // Limpiar estructura 
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(iport); // Cargar el num de puerto en la estructura (ajustar al orden estandar de bytes de la red mediante htons)
    if(inet_pton(AF_INET,argv[1],&servaddr.sin_addr) <= 0) // Verificar y asignar IPV4
    {
        printf("Falla al convertir dir IP a binario\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Conectarse al servidor y handlear errores
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(connect (sockfd, (SA *) &servaddr, sizeof(servaddr)) < 0)
    {
        printf("Falla al conectar con el servidor\n");
        exit(EXIT_FAILURE); 
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Construir el mensaje a enviar
    //--------------------------------------------------------------------------------------------------------------------------------------------
    memset(sendmsg, 0 , MAXLINE); 
    strcat(sendmsg,"Tipo C | Descargar Archivo\n");

    // Enviar el mensaje
    //--------------------------------------------------------------------------------------------------------------------------------------------
    sendBytes = strlen(sendmsg); // Calcular la cantidad de caracteres del mensaje
    WriteReturnValue = write(sockfd, sendmsg, sendBytes);
    if((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
    {
        printf("Falla al enviar mensaje\n");
        exit(EXIT_FAILURE); 
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Recibir e imprimir respuesta
    //--------------------------------------------------------------------------------------------------------------------------------------------
    memset(recvline, 0 , MAXLINE); 
    readBytes = 0;
    while((recvline[readBytes - 1] != '\n') && (recvline[readBytes - 2] != '\r')) // Detectar final de la querry
    {
        while(readBytes <= 0) // Esperar hasta que haya datos
        {
            readBytes = recv(sockfd, recvline, MAXLINE-1, MSG_DONTWAIT);
        }
    }
    //printf("Recibido tam: %s\n",recvline);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar errores de lectura
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(readBytes < 0)
    {
        printf("Error de lectura\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Notificar tam recibido
    //--------------------------------------------------------------------------------------------------------------------------------------------
    memset(sendmsg, 0 , MAXLINE); 
    strcat(sendmsg,"Tam recibido\n");
    sendBytes = strlen(sendmsg); // Calcular la cantidad de caracteres del mensaje
    //printf("El tam del ack es %ld\n",sendBytes);
    WriteReturnValue = write(sockfd, sendmsg, sendBytes);
    if((WriteReturnValue == -1) || ((long unsigned int)WriteReturnValue != sendBytes))
    {
        printf("Falla al enviar mensaje\n");
        exit(EXIT_FAILURE); 
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Obtener del mesnaje el tam del archivo
    //--------------------------------------------------------------------------------------------------------------------------------------------
    char fileSize[MAXLINE];
    strcpy(fileSize,recvline);
    fileSize[strcspn(fileSize,"\n")] = 0; 
    long unsigned int File_Size = (long unsigned int)atoi(fileSize);
    //printf("El tam del archivo es %ld\n",File_Size);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Descargar archivo
    //--------------------------------------------------------------------------------------------------------------------------------------------
    descargarArchivo(&sockfd,File_Size);
    //printf("Archivo descargado\n");
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Probar que el archivo recibido es la base de datos correcta
    //--------------------------------------------------------------------------------------------------------------------------------------------
    querryDePrueba();
    //--------------------------------------------------------------------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------------------------------------------------------------------
    close(sockfd);      // Cerrar el socket
    exit(EXIT_SUCCESS); // Salir
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void descargarArchivo(int *socket,unsigned long int file_size)
{
    // Variables
    //--------------------------------------------------------------------------------------------------------------------------------------------
    char recvline[MAXLINE];
    long int rcvBytes = 0;
    long int totalRcvBytes = 0;
    long int BytesWritten = 0;
    unsigned long int fileSIZE = file_size;
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Abrir/crear Archivo
    //--------------------------------------------------------------------------------------------------------------------------------------------
    //int target = open(FILEPATH,O_RDWR | O_CREAT | O_APPEND, 0664);
    int target = open(FILEPATH,O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if(target == -1)
    {
        printf("Error al hacer/leer el archivo\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Recibir y escribir sobre el archivo hasta que se hayan recibido todos los bytes de este
    //--------------------------------------------------------------------------------------------------------------------------------------------
    while(fileSIZE != 0)
    {
        memset(recvline, 0, MAXLINE);

        rcvBytes = read(*(socket),recvline, MAXLINE);        
        fileSIZE -= (unsigned long int)rcvBytes;
        totalRcvBytes += rcvBytes;
        //printf("Recibidos %ld Bytes\n", totalRcvBytes);

        BytesWritten += write(target,recvline,(unsigned long int)rcvBytes);
        //printf("Escritos %ld Bytes\n",BytesWritten);
    }
    //printf("Termine de escribir el archivo\n");
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Cerrar archivo y handlear error
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((close(target) < 0))
    {
        printf("Error all cerrar conn\n");
        exit(EXIT_FAILURE); 
    } 
    //--------------------------------------------------------------------------------------------------------------------------------------------

}

void VerificarArgumentosClientINET(int argc, char *argv[])
{
    // Verificar cantidad de argumentos
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(argc != 3)  
    {
        printf("cantidad de argumentos incorrecta\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que el puerto ingresado en los argumentos sea correcto
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int i = 0; i < strlen(argv[2]); i ++)
    {
        if((isdigit(argv[2][i]) == 0)|| (atoi(argv[2]) <= 0) || (atoi(argv[2]) > 65535)) // Verificar que en el argumento para el puerto se haya
        {                                                                // ingresado un num y no letras ni caracteres especiales
            printf("Debe ingresar un puerto correcto\n");                // y que este num sea > = 0 y < 65535
            exit(EXIT_FAILURE);
        }
    }                                           // El valor se escribe en iport LUEGO de verrificar que el valor sea correcto ya el valor ingresado 
    //                                             en el argumento puede exceder el valor max del short unsigned int
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void querryDePrueba()
{
    sqlite3 *db;    // esto es la base de datos en el code
    //char *err_msg = 0;
    sqlite3_stmt *res;
    
    int rc = sqlite3_open("test.db", &db); // abrir la base de datos (archivo "test.db", si no existe lo crea) (int rc es solo para recibir success o no)
    
    if (rc != SQLITE_OK)  // Handlear error
    {
        
        fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
        printf("No se puede abrir la db. El error es %s\n",sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(EXIT_FAILURE);
    }

    printf("Realizando querry...\n"); 

    rc = sqlite3_prepare_v2(db, "SELECT Name FROM Cars", -1, &res, 0);  // Hacer querry SELECT  

    if (rc != SQLITE_OK) {
        
        fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
        exit(EXIT_FAILURE);
    }

    printf("Resultados obtenidos!\n");
    printf("Display resultados:\n");

    while(sqlite3_step(res) != SQLITE_DONE)
    {
        int i;
        int num_cols = sqlite3_column_count(res);
        //printf("Hay %d columnas\n",num_cols);
        for (i = 0; i < num_cols; i++)
		{
			switch (sqlite3_column_type(res, i))
			{
			case (SQLITE3_TEXT):
				printf("%s, ", sqlite3_column_text(res, i));
				break;
			case (SQLITE_INTEGER):
				printf("%d, ", sqlite3_column_int(res, i));
				break;
			case (SQLITE_FLOAT):
				printf("%g, ", sqlite3_column_double(res, i));
				break;
			default:
				break;
			}
		}
		printf("\n");
    }
}
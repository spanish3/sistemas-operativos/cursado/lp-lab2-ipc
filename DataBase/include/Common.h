#ifndef COMMON_H    // Comienzo del ifdef guard
#define COMMON_H_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> // write, close, etc
#include <ctype.h> // isdigit()
#include <arpa/inet.h> // inet_pton()
#include <sys/un.h> // direciiones unix
#include <net/if.h> // INET6
#include <netinet/in.h> // INET6
#include <pthread.h>
#include <errno.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <sqlite3.h>
#include <sys/stat.h> // stat para tam de archivo
#include <fcntl.h> // open() para archivos

#include "db_functions.h"
#include "TaskHandlingThread.h"
#include "ConfigSocketINET.h"
#include "Arg-Verify-Server.h"
#include "TaskHandlingThread.h"
#include "Server_INET_Stream.h"

#define DB_CONETCIONS 5  
#define MAXLINE 4096
#define SA struct sockaddr

struct local_threads_arg_struct {
    int id;
    int *ConnSocket;
    int *Handlers;
    pthread_mutex_t *Handler_lock;
    pthread_mutex_t *Database_lock;
    int *lastid;
    sqlite3 **db;   
    int *salir;
};

struct INET_arg_struct {
    char IPV4_Server_Address[MAXLINE];
    short unsigned int IPV4_iport;
    sqlite3 **db;
    int maxClientes;
    int *salir;
};

    int isValidIPAddress(char *ipAddr);
    int my_strlen(char *c);
    void clearBuffer();
    int safeGetString(char *string, int max);
    void ocuparHandler(int *Handlers, int i, pthread_mutex_t *lock);
    void liberarHandler(int *Handlers, int i, pthread_mutex_t *lock);
    int getFirstAvailableHandler(int *Handlers, long unsigned int maxHandlers);
    int getAvailableHandlersAmount(int *Handlers, long unsigned int maxHandlers);
    char getTypeAndMessege(char *string, char *msg);
    char const * errnoname(int errno_);

#endif // Fin del ifdef guard
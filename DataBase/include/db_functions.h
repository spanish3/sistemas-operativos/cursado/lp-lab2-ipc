#ifndef DBFUNCTIONS_H    // Comienzo del ifdef guard
#define DBFUNCTIONS_H_H

void CreateAndFillDB(sqlite3 **db, char *err_msg);
void getQuerryResponse(int rc, sqlite3 **db, char *querry, char *response, pthread_mutex_t *lock);
void AddMsgToDB(sqlite3 **db, char *mensaje, int *lastid);

#endif // Fin del ifdef guard
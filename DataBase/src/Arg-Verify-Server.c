#include "Common.h"

void verificarArgumentos(int argc, char *argv[])
{
    // Verificar cantidad de argumentos correcta, luego realizar las verificaciones para los argumentos de cada protocolo
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(argc != 3)
    {
        printf("Cantidad de argumentos incorrecta\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    VerificarArgumentosINET(argv);
}

void VerificarArgumentosINET(char *argv[])
{
    // Verificar que la IPV4 Ingresada sea correcta
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(!isValidIPAddress(argv[1])) 
    {
        printf("Debe ingresar una dir IPV4 correcta\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que el puerto ingresado para IPV4 sea un este compuesto de digitos y tenga un valor correcto
    for(unsigned int i = 0; i < strlen(argv[2]); i ++)
    {
        if((isdigit(argv[2][i]) == 0)|| (atoi(argv[2]) <= 0) || (atoi(argv[2]) > 65535)) 
        {                                                                // Verificar que en el argumento para el puerto se haya
            printf("Debe ingresar un puerto correcto\n");                // ingresado un num y no letras ni caracteres especiales
            exit(EXIT_FAILURE);                                          // y que este num sea > = 0 y < 65535
        }
    }                                           // El valor se escribe en iport LUEGO de verificar que el valor sea correcto ya el valor ingresado 
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

#include "Common.h"

void *INET_Server_Code(void *arg)
{
    struct INET_arg_struct *arguments = arg; // Recuperar argumentos
    
    // Variables: Crear / configurar sockets
    //--------------------------------------------------------------------------------------------------------------------------------------------
    int listenfd; // File descriptor para el socket que queda a escucha de conexiones.
    int *connfd; // File descriptor para el socket que proviene de accept() (conection established)
    struct sockaddr_in servaddr; // Estructura para espesificar el server address

    connfd = malloc((unsigned long int)arguments->maxClientes * sizeof(int));
    for(int i = 0; i < arguments->maxClientes; i++)
    {
        connfd[i] = -1;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Variable para obtener el siguiente id de mensajes obtenidos
    //--------------------------------------------------------------------------------------------------------------------------------------------
    int lastid = 0;
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Variables: Crear hilos para handlear las conexiones | Asignar memoria: Tantos TaskThreads, CountingThreads y argumentos como maxClientes
    //--------------------------------------------------------------------------------------------------------------------------------------------
    pthread_t *TaskThread;
    pthread_mutex_t Handler_lock = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_t Database_lock = PTHREAD_MUTEX_INITIALIZER;
    struct local_threads_arg_struct *Handler_Thread_Args;
    
    TaskThread = malloc((unsigned long int)arguments->maxClientes * sizeof(pthread_t)); 
    Handler_Thread_Args = malloc((unsigned long int)arguments->maxClientes * sizeof(struct local_threads_arg_struct));
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Variables: Limitar la cantidad de hilos handlers que se pueden lanzar y determinar el primero disponible. Asignar memoria
    //--------------------------------------------------------------------------------------------------------------------------------------------
    long unsigned int CurrentAHAmount = 0; // Cantidad de handlers disponibles en determinado momento
    int *AvailableHandlers; // Array de int que sirve como banderas, si AvailableHandlers[i] == 1 -> el handler i esta dispponible
    int nextHandler; // Este int se valua con el primer handler que se encuentre disponible en determinado momento

    AvailableHandlers = malloc((unsigned long int)arguments->maxClientes * sizeof(int)); // Asginar memoria: El array tendra tantos elementos como maxClientes
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Configurar socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    ServerConfigSocketINET(&listenfd, &servaddr, arguments->IPV4_iport, (unsigned long int)arguments->maxClientes, arguments->IPV4_Server_Address);
    //--------------------------------------------------------------------------------------------------------------------------------------------
     
    // Inicialmente todos los handlers estan disponibles, llenar el array de indicadores con "1" -> "Disponible"
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(int i = 0; i < arguments->maxClientes; i++)
    {
        AvailableHandlers[i] = 1;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // El hilo para INET se encuentra siempre esperando conexiones y lanzando hilos para que se encarguen de estas cuando se establecen
    //--------------------------------------------------------------------------------------------------------------------------------------------
    while(*(arguments->salir) == 0) // Si se ingresa "salir" en main, ya no se esperan conexiones
    {   
        // Obtener cantidad de handlers disponibles. Si ya hay tantos hilos trabajando como maxClientes, se debe espearar hasta que uno termine
        //----------------------------------------------------------------------------------------------------------------------------------------
        pthread_mutex_lock(&Handler_lock); // Se accede en ex mutua, pues los hilos handlers modifican AvailableHandlers liberando un handler al terminar
        CurrentAHAmount = (long unsigned int)getAvailableHandlersAmount(AvailableHandlers,(unsigned long int)arguments->maxClientes);
        pthread_mutex_unlock(&Handler_lock);

        if(CurrentAHAmount == 0 || getFirstAvailableHandler(AvailableHandlers,(unsigned long int)arguments->maxClientes) < 0)
        {
            //printf("No hay conexiones disponibles, espere hasta que una se desocupe\n");
            while(CurrentAHAmount == 0)
            {
                if(*(arguments->salir) == 0)
                {
                    pthread_mutex_lock(&Handler_lock);
                    CurrentAHAmount = (long unsigned int)getAvailableHandlersAmount(AvailableHandlers,(unsigned long int)arguments->maxClientes);
                    pthread_mutex_unlock(&Handler_lock);
                }
                else
                {
                    break;
                }
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------------
        
        if(*(arguments->salir) == 0)
        {
            // Sabiendo que hay handlers disponibles, obtener el primero disponible en orden numerico
            //------------------------------------------------------------------------------------------------------------------------------------
            pthread_mutex_lock(&Handler_lock);
            nextHandler = getFirstAvailableHandler(AvailableHandlers,(unsigned long int)arguments->maxClientes);
            pthread_mutex_unlock(&Handler_lock);
            //------------------------------------------------------------------------------------------------------------------------------------

            // Espera una conexion y handlear el error en caso de que ocurra. Se utilizara el fd conn[nextHandler] -> primer handler disponible
            //------------------------------------------------------------------------------------------------------------------------------------
            int flags = fcntl(listenfd, F_GETFL, 0);       // Configurar para que accept() pase a ser
            fcntl(listenfd, F_SETFL, flags | O_NONBLOCK);  // no bloqueante.

            while(connfd[nextHandler] < 0)
            {
                if(*(arguments->salir) == 0)
                {
                    connfd[nextHandler] = accept(listenfd, (SA *) NULL, NULL); // NULL -> No importa quien se conecte, aceptar la conexión
                    /*if(connfd[nextHandler] == -1) // Ya no chequeo el error poque mi accept es no bloqueante y espero en este while
                    {                               // hasta que se establezca una conexion correcta
                        printf("Error en accept()\n");
                        exit(EXIT_FAILURE);
                    }*/
                }
                else
                {
                    break;
                }
            }
            //------------------------------------------------------------------------------------------------------------------------------------
            
            // Ahora que hay conexion, se llenan los argumentos, se lanza el hilo handler de esta conexion y se modifica AvalableHandlers en ex mutua
            //------------------------------------------------------------------------------------------------------------------------------------
            //printf("Conexion con %d establecida. Manejada por el hilo %d\n",connfd[nextHandler],nextHandler); 
            
            Handler_Thread_Args[nextHandler].id = nextHandler;
            Handler_Thread_Args[nextHandler].ConnSocket = &(connfd[nextHandler]);
            Handler_Thread_Args[nextHandler].Handlers = AvailableHandlers;
            Handler_Thread_Args[nextHandler].Handler_lock = &Handler_lock;
            Handler_Thread_Args[nextHandler].Database_lock = &Database_lock;
            Handler_Thread_Args[nextHandler].lastid = &lastid;
            Handler_Thread_Args[nextHandler].db = arguments->db;
            Handler_Thread_Args[nextHandler].salir = arguments->salir;
            
            pthread_create(&(TaskThread[nextHandler]),NULL,Task,&(Handler_Thread_Args[nextHandler]));

            ocuparHandler(AvailableHandlers,nextHandler,&Handler_lock);
            //------------------------------------------------------------------------------------------------------------------------------------
        }
        else
        {
            break;
        }   
    }

    // Si se desea salir del servidor, esperar a todos los hilos lanzados por este hilo INET (estos hilos terminan si salir == 1)
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(*(arguments->salir) == 1)
    {
        for(int i = 0; i < arguments->maxClientes; i++)
        {
            pthread_join(TaskThread[i],NULL);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Luego cerrar los fd, verificar errores y liberar toda la memoria asignada
    //--------------------------------------------------------------------------------------------------------------------------------------------          
    if((close(listenfd) < 0)) // Cerrar fd de escucha de conexiones
    {
        printf("Error all cerrar listenfd\n");
        exit(EXIT_FAILURE); 
    }
    free(AvailableHandlers);   // Liberar array indicador de handlers disponibles
    free(TaskThread);          // Liberar array de hilos para manejar conexiones 
    free(connfd);              // Liberar array de fd para conexiones
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    return NULL;
}
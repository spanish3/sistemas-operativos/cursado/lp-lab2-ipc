#include "Common.h"

#define FILEPATH "test.db"

void* Task(void * arg)
{
    // Variables
    //------------------------------------------------------------------------------------------------------------------------------------------------
    struct local_threads_arg_struct *arguments = arg;
    
    char recvline[MAXLINE+1]; // Buffer para recibir. Es MAXLINE + 1 porque...
    char aux[MAXLINE];         // Auxiliar para quitar el checksum y el end of messege
    char rawmsg[MAXLINE] = "";      // Mensaje sin checksum ni end of line
    char fileSize[MAXLINE] = ""; // Construir mensaje para enviar tam del archivo
    char buffer[MAXLINE] = ""; // Enviar archivo
    long int readBytes; // Enviar archivo
    long int writeBytes; // Enviar archivo

    char recvline2[MAXLINE+1]; // Recibir ack
    long int readBytes2; // Recibir ack

    int rc = 0; // Para succes o error de las operaciones con DB
    char querry[MAXLINE] = "";
    char response[MAXLINE];
    //------------------------------------------------------------------------------------------------------------------------------------------------
    
    while(*(arguments->salir) == 0) 
    {
        // Leer mensaje: Inicialmente readBytes es cero, siempre y cuando no se desee salir, se llama a recv. Si readBytes > 0 -> se ha 
        // recibido un dato.
        //--------------------------------------------------------------------------------------------------------------------------------------------
        readBytes = 0;
        while(readBytes <= 0)
        {
            if(*(arguments->salir) == 0) 
            {
                readBytes = recv(*(arguments->ConnSocket), recvline, MAXLINE-1, MSG_DONTWAIT);
            }
            else  // Si se acaba el tiempo o se produce un error de lectura (que puede ser pot timeout), salir
            {
                /*if(readBytes < 0)
                {
                    // Se reciben erores EAGAIN por timeout
                    printf("Error de lectura en task %d\n",arguments->id);
                    printf("El errno es %s\n",errnoname((int)errno));
                } */
                if(close(*(arguments->ConnSocket)) < 0)
                {
                    printf("Error al cerrar la conec %d\n",*(arguments->ConnSocket));
                }
                break;
            }
        }
        //--------------------------------------------------------------------------------------------------------------------------------------------

        // Una vez recibido el mensaje (siempre y cuando no se haya indicado salir), evaluar de que tipo de cliente viene y resolver segun corresponda
        //--------------------------------------------------------------------------------------------------------------------------------------------
        if(*(arguments->salir) == 0)
        {
            while(readBytes > 0)
            {
                if(*(arguments->salir) == 0)
                {
                    if(recvline[readBytes - 1] == '\n') // El final de los mensajes http suele tener \n\r\n\r, al detectar un \n podemos intuir 
                    {                                   // que es el final del mensaje.
                        // Borrar contenido del string (colocar ceros) para que no quede parte de mensajes anteriores
                        //----------------------------------------------------------------------------------------------------------------------------
                        memset(aux, 0, MAXLINE);    
                        memset(rawmsg, 0, MAXLINE);
                        //----------------------------------------------------------------------------------------------------------------------------          

                        // Obtener la querry sin el tipo de mensaje ni '\n' y obtener el tipo de mensaje en un char 'a', 'b' o 'c'
                        //----------------------------------------------------------------------------------------------------------------------------
                        strcpy(aux,recvline);
                        strcpy(rawmsg,aux);
                        rawmsg[strcspn(rawmsg,"\n")] = 0;
                        char t = getTypeAndMessege(aux, querry);
                        //----------------------------------------------------------------------------------------------------------------------------

                        // Agregar el mensaje recibido a tabla de mensajes de la db
                        //----------------------------------------------------------------------------------------------------------------------------
                        char msgAddDB[MAXLINE];
                        strcpy(msgAddDB,rawmsg);
                        msgAddDB[strcspn(msgAddDB,"\n")] = 0;
                        pthread_mutex_lock(arguments->Database_lock);
                        AddMsgToDB(arguments->db, msgAddDB, arguments->lastid);
                        pthread_mutex_unlock(arguments->Database_lock);
                        //----------------------------------------------------------------------------------------------------------------------------

                        // En caso de que el mensaje provenga de un cliente tipo a o b, realizar la querry y enviar la respuesta
                        //----------------------------------------------------------------------------------------------------------------------------
                        if((t == 'a') || (t == 'b'))
                        {
                            //printf("Realizando querry...\n"); 
                            getQuerryResponse(rc,arguments->db, querry, response,arguments->Database_lock);
                            //printf("Se obtuvo de la querry: %s\n",response);
                            strcat(response,"\r\n"); // Agregar \r\n para detectar final de querry desde el server y el cliente
                            writeBytes = write(*(arguments->ConnSocket), response, strlen(response)); // Enviar respuesta
                            if(writeBytes < 0) // Handlear error
                            {
                                printf("Error de escritura\n");
                                exit(EXIT_FAILURE); 
                            }
                        }
                        //----------------------------------------------------------------------------------------------------------------------------

                        // Si el mensaje proviene de un cliente tipo C se debe realizar el handshake: tam-> ; <-ack ; -> archivo
                        //----------------------------------------------------------------------------------------------------------------------------
                        if(t == 'c')
                        {
                            // Obtener el tamaño del archivo en bytes
                            //------------------------------------------------------------------------------------------------------------------------
                            struct stat sb;
                            long int fileBytes;

                            if(stat(FILEPATH,&sb))
                            {
                                perror("stat");
                                exit(EXIT_FAILURE);
                            }
                            else
                            {
                                fileBytes = sb.st_size;
                            }
                            //------------------------------------------------------------------------------------------------------------------------
                            
                            // Enviar tam del archivo
                            //------------------------------------------------------------------------------------------------------------------------
                            sprintf(fileSize,"%ld",fileBytes);
                            strcat(fileSize,"\r\n");
                            writeBytes = write(*(arguments->ConnSocket), fileSize, strlen(fileSize)); 
                            if(writeBytes < 0)
                            {
                                printf("Error de escritura\n");
                                exit(EXIT_FAILURE); 
                            }
                            //------------------------------------------------------------------------------------------------------------------------

                            // Recibir ack de tam
                            //------------------------------------------------------------------------------------------------------------------------
                            readBytes2 = 0;
                            memset(recvline2, 0, MAXLINE); 
                            while(readBytes2 <= 0)
                            {
                                if(*(arguments->salir) == 0) 
                                {
                                    readBytes2 = recv(*(arguments->ConnSocket), recvline2, MAXLINE-1, MSG_DONTWAIT);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            // printf("Recibido ack: %s",recvline2);
                            //------------------------------------------------------------------------------------------------------------------------

                            // Enviar Archivo
                            //------------------------------------------------------------------------------------------------------------------------
                            int input_file = open(FILEPATH, O_RDONLY); // Abrir archivo
                            long int sentBytes = 0;
                    
                            while (fileBytes > 0) 
                            {
                                if(*(arguments->salir) == 0) 
                                {
                                    // Read data into buffer.  We may not have enough to fill up buffer, so we
                                    // store how many bytes were actually read in bytes_read.
                                    long int bytes_read = read(input_file, buffer, sizeof(buffer));
                                    if (bytes_read < 0) // handle errors
                                    {
                                        printf("Error al leer archivo\n");
                                        exit(EXIT_FAILURE);
                                    }

                                    long int bytes_written = write(*(arguments->ConnSocket), buffer, (unsigned long int)bytes_read);
                                    if (bytes_written <= 0) 
                                    {
                                        printf("Error al enviar\n");
                                        exit(EXIT_FAILURE);
                                    }

                                    fileBytes -= bytes_written;
                                    sentBytes += bytes_written;
                                    //printf("He enviado %ld Bytes\n",sentBytes);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            close(input_file);
                            //------------------------------------------------------------------------------------------------------------------------
                        }
                        //----------------------------------------------------------------------------------------------------------------------------
                        
                        // Si el mensaje no tiene el formato TIPO A | mensjae o TIPO B | Mensaje o TIPO C | Mensaje, no se reconoce un tipo de cliente
                        //----------------------------------------------------------------------------------------------------------------------------
                        if(t == 'd')
                        {
                            printf("No se ha espesificado el tipo de cliente");
                        }
                        //----------------------------------------------------------------------------------------------------------------------------

                        readBytes = 0;
                        break;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        //--------------------------------------------------------------------------------------------------------------------------------------------        
    }
    
    liberarHandler(arguments->Handlers,arguments->id,arguments->Handler_lock);
    //printf("Termina task: %d\n",arguments->id);
    return NULL;
}
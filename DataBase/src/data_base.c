#include "Common.h"

int main(int argc, char *argv[])
{    
    // Variables: Base de datos
    //--------------------------------------------------------------------------------------------------------------------------------------------
    sqlite3 *db;    // esto es la base de datos en el code
    char *err_msg = 0;
    //sqlite3_stmt *res;
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Crear y llenar base de datos
    //--------------------------------------------------------------------------------------------------------------------------------------------
    CreateAndFillDB(&db,err_msg);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Variables: Server INET6
    //--------------------------------------------------------------------------------------------------------------------------------------------
    pthread_t INET_Server_Thread;
    struct INET_arg_struct INET_arguments;
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Variables: Mecanismo para salir del programa
    //--------------------------------------------------------------------------------------------------------------------------------------------
    char salir[MAXLINE];
    int exitAllThreads = 0;
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar argumentos y llenar estructura de argumentos para los hilos
    //--------------------------------------------------------------------------------------------------------------------------------------------
    verificarArgumentos(argc,argv);

    strcpy(INET_arguments.IPV4_Server_Address,argv[1]);
    INET_arguments.IPV4_iport = (short unsigned int)atoi(argv[2]);
    INET_arguments.db = &db;
    INET_arguments.maxClientes = DB_CONETCIONS;
    INET_arguments.salir = &exitAllThreads;
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Lanzar hilos 
    //--------------------------------------------------------------------------------------------------------------------------------------------
    //INET
    pthread_create(&INET_Server_Thread,NULL,INET_Server_Code,&INET_arguments);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Entrar a un bucle infinito para que el programa siga corriendo hasta que el usuario decida finalizarlo
    //--------------------------------------------------------------------------------------------------------------------------------------------
    while(!exitAllThreads)
    {
        // Para determinar si el usuario desea salir, se recibe un string por stdin
        //----------------------------------------------------------------------------------------------------------------------------------------
        printf("Ingrese 'salir' en cualquier momento para cerrar el programa servidor\n");
        safeGetString(salir,MAXLINE);
        if(!strcmp(salir,"salir\n"))
        {
            exitAllThreads = 1; // -> Salir
            // Antes de salir esperar hasta que todos los otros hilos terminen de ejecutarse
            //----------------------------------------------------------------------------------------------------------------------------------------
            pthread_join(INET_Server_Thread,NULL);
            //----------------------------------------------------------------------------------------------------------------------------------------
        }
        else
        {
            printf("Ha ingresado %s\n",salir);
        }
        //----------------------------------------------------------------------------------------------------------------------------------------
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
}
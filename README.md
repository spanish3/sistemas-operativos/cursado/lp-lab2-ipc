# lp-lab2-IPC: Arquitectura Cliente-Servidor de Tres Niveles

## Resumen
Este proyecto implementa una arquitectura cliente-servidor de tres niveles utilizando mecanismos de IPC en C. El sistema consta de un servidor, múltiples clientes y una base de datos SQLite para facilitar la comunicación entre procesos. Los clientes interactúan con el servidor para ejecutar consultas personalizadas, y el servidor gestiona la comunicación entre los clientes y la base de datos.

![Diagrama de Bloques](img/connectionPool.png)

## Comenzar
```
    cd existing_repo
    git remote add origin https://gitlab.com/english6060242/operating-systems/coursework/lp-lab2-ipc.git
    git branch -M main
    git push -uf origin main

```


## Estructura del Proyecto
El proyecto se divide en tres componentes principales:

### 1. Servidor
- **Funcionalidad:**
  - Mantiene cinco conexiones concurrentes con la base de datos durante todo su ciclo de vida.
  - Gestiona las conexiones y desconexiones de los clientes.
  - Actúa como un relé para los mensajes de usuario hacia la base de datos.
  - Implementa lógica y diseño personalizados según los requisitos del estudiante.

### 2. Clientes
- **Tres Tipos de Clientes:**
  1. **Cliente A:**
     - Envía periódicamente una consulta predefinida y muestra el resultado.
  2. **Cliente B:**
     - Acepta la entrada del usuario para consultas a través de la CLI y muestra los resultados.
  3. **Cliente C:**
     - Descarga un archivo de la base de datos al host local.

### 3. Base de Datos
- **Base de Datos SQLite:**
  - Almacena mensajes enviados por clientes en una tabla dedicada "Mensajes".
  - Utiliza mecanismos de IPC para manejar conexiones con el servidor.

## Compilación y Uso
- Compila el código con las siguientes banderas: `-Wall -Pedantic -Werror -Wextra -Wconversion -std=gnu11`
- Asegura una correcta gestión de memoria y una estructura de código modular.
- Utiliza el `Makefile` proporcionado para la compilación.

## Requisitos
- Todo el código debe estar libre de errores y advertencias.
- La gestión de memoria debe seguir las mejores prácticas.
- El código debe ser modular, estar bien organizado y seguir un estilo consistente.
- La gestión adecuada de errores es obligatoria.
- No se debe usar características marcadas por `cppcheck`.

## Cómo Ejecutar
1. Clona el repositorio: `git clone https://gitlab.com/english6060242/operating-systems/coursework/lp-lab2-ipc.git`
2. Usa tres terminales y navega a DataBase, Server y Clients respectivamente.
3. En cada terminal, compila el código: `make`
4. Ejecuta 'make test' para la base de datos y luego para el servidor.
5. En cuanto a los clientes, utiliza los comandos make individuales o ejecuta el archivo bashCode.sh (gestiona los permisos primero).

## Reporte
Para una explicación detallada del desarrollo del proyecto, elecciones de diseño y justificación, consulta el documento de informe proporcionado en el repositorio.

## Licencia
Este proyecto está licenciado bajo la [Licencia MIT](LICENSE.md).
#ifndef COMMONFUNCTIONS_H    // Comienzo del ifdef guard
#define COMMONFUNCTIONS_H_H

    int isValidIPAddress(char *ipAddr);
    int isValidIPV6Address(char *ipAddr);
    int my_strlen(char *c);
    void clearBuffer();
    int safeGetString(char *string, int max);
    int isValidFilename(char *string);
    void ocuparHandler(int *Handlers, int i, pthread_mutex_t *lock);
    void liberarHandler(int *Handlers, int i, pthread_mutex_t *lock);
    int getFirstAvailableHandler(int *Handlers, long unsigned int maxHandlers, pthread_mutex_t *lock);
    int getAvailableHandlersAmount(int *Handlers, long unsigned int maxHandlers, pthread_mutex_t *lock);
    char getTypeAndMessege(char *string, char *msg);
    char const * errnoname(int errno_);
    
#endif // Fin del ifdef guard
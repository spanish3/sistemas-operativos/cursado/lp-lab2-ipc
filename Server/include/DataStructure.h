#ifndef DATASTRUCTURE_H    // Comienzo del ifdef guard
#define DATASTRUCTURE_H_H

#define DB_CONETCIONS 5  
#define MAXLINE 4096
#define SA struct sockaddr

struct db_req
{
    int id;
    int *conn;
    char *sendmsg;
    struct db_req * next;
};

typedef struct db_req db_request;

typedef struct db_req_list db_request_list;
struct db_req_list
{
    db_request *node;
};

/*struct local_threads_arg_struct {
    int id;
    int *ConnSocket;
    int *Handlers;
    pthread_mutex_t *Handler_lock;
    pthread_mutex_t *Database_lock;
    sqlite3 **db;   
    int *salir;
};*/

/*struct INET_arg_struct {
    char IPV4_Server_Address[MAXLINE];
    short unsigned int IPV4_iport;
    sqlite3 **db;
    int maxClientes;
    int *salir;
};*/

struct ack_arg_struct{
    int *ConnSocket;
    int *ack;
    pthread_mutex_t *ack_lock;
};

struct Pool_arg_struct {
    char IPV4_Server_Address[MAXLINE];
    short unsigned int IPV4_iport;
    struct ack_arg_struct *ack_arg;
    pthread_mutex_t *req_list_lock;
    db_request_list *list;
    int *salir;
};

struct Conection_arg_struct {
    int id;
    char IPV4_Server_Address[MAXLINE];
    short unsigned int IPV4_iport;
    int *availableConections;
    pthread_mutex_t *conections_lock;
    pthread_mutex_t *list_lock;
    db_request_list *list;
    struct ack_arg_struct *ack_arg;
    int *salir;
};

struct INET_arg_struct {
    char IPV4_Server_Address[MAXLINE];
    short unsigned int IPV4_iport;
    pthread_mutex_t *req_list_lock;
    db_request_list *list;
    struct ack_arg_struct *ack_arg;
    int maxClientes;
    int *salir;
};

struct UNIX_arg_struct {
    char UNIX_File_Name[MAXLINE];
    pthread_mutex_t *req_list_lock;
    db_request_list *list;
    int maxClientes;
    int *salir;
};

struct INET6_arg_struct {
    char IPV6_Server_Address[MAXLINE];
    short unsigned int IPV6_iport;
    char IPV6_Interface[MAXLINE];
    pthread_mutex_t *req_list_lock;
    db_request_list *list;
    int maxClientes;
    int *salir;
};

struct local_threads_arg_struct {
    int id;
    int checksegs;
    int *ConnSocket;
    int ExitThread;
    int *Handlers;
    struct ack_arg_struct *ack_arg;
    pthread_mutex_t *req_list_lock;
    pthread_mutex_t *handlers_lock;
    pthread_mutex_t *lock;
    db_request_list *list;
    int *salir;
};

db_request *new_db_request(int id, int *conn, char *sendmsg);

db_request_list *new_db_request_list();

void add_db_request(db_request_list *self, int id, int *conn, char *sendmsg);

/*void addExisitngPublisher(PublisherList *self, Publisher *p);

void removeHeadPublisher(PublisherList *self);*/

void remove_db_request(db_request_list *self, int id);

void remove_req_list_head(db_request_list *self);

db_request *get_db_request(db_request_list *pl, int id);

void print_db_request_list(db_request_list self);

void delete_db_request_list(db_request_list *self);

/*int PubExists(PublisherList *pl, int c);

int getPublisherIndex(PublisherList *self, int c);*/

int isEmpty_db_request_list(db_request_list *self);

int db_request_list_getNextID(db_request_list *self);

#endif // Fin del ifdef guard
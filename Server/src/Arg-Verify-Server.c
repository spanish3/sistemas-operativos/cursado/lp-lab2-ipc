#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"
#include "Arg-Verify-Server.h"

void verificarArgumentos(int argc, char *argv[])
{
    // Verificar cantidad de argumentos correcta, luego realizar las verificaciones para los argumentos de cada protocolo
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(argc != 10)
    {
        printf("Cantidad de argumentos incorrecta\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    VerificarArgumentosINET(argv);
    VerificarArgumentosUNIX(argv);
    VerificarArgumentosINET6(argv);

    // Verificar que la cantidad de clientes ingresada sea un num positivo y menor al max
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int i = 0; i < strlen(argv[9]); i ++)
    {
        if((isdigit(argv[9][i]) == 0)|| (atoi(argv[9]) < 0) || (atoi(argv[9]) > 10000) || strlen(argv[9]) > sizeof(int)) // Verificar que no se hayan ingresado letras, 
        {                                                                               // caracteres especiales ni num negativos
            printf("Debe ingresar un puerto correcto\n");                
            exit(EXIT_FAILURE);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void VerificarArgumentosINET(char *argv[])
{
    // Verificar que la IPV4 Ingresada sea correcta
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(!isValidIPAddress(argv[1])) 
    {
        printf("Debe ingresar una dir IPV4 correcta\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que el puerto ingresado para IPV4 sea un este compuesto de digitos y tenga un valor correcto
    for(unsigned int i = 0; i < strlen(argv[2]); i ++)
    {
        if((isdigit(argv[2][i]) == 0)|| (atoi(argv[2]) <= 0) || (atoi(argv[2]) > 65535)) 
        {                                                                // Verificar que en el argumento para el puerto se haya
            printf("Debe ingresar un puerto correcto\n");                // ingresado un num y no letras ni caracteres especiales
            exit(EXIT_FAILURE);                                          // y que este num sea > = 0 y < 65535
        }
    }                                           // El valor se escribe en iport LUEGO de verificar que el valor sea correcto ya el valor ingresado 
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que la IPV4 Ingresada sea correcta
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(!isValidIPAddress(argv[7])) 
    {
        printf("Debe ingresar una dir IPV4 correcta\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que el puerto ingresado para IPV4 sea un este compuesto de digitos y tenga un valor correcto
    for(unsigned int i = 0; i < strlen(argv[8]); i ++)
    {
        if((isdigit(argv[8][i]) == 0)|| (atoi(argv[8]) <= 0) || (atoi(argv[8]) > 65535)) 
        {                                                                // Verificar que en el argumento para el puerto se haya
            printf("Debe ingresar un puerto correcto\n");                // ingresado un num y no letras ni caracteres especiales
            exit(EXIT_FAILURE);                                          // y que este num sea > = 0 y < 65535
        }
    }                                           // El valor se escribe en iport LUEGO de verificar que el valor sea correcto ya el valor ingresado 
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

void VerificarArgumentosUNIX(char *argv[])
{
    // Verificar que el nombre archivo para la conexion UNIX sea correcto
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((strlen(argv[3])>MAXLINE) || (!isValidFilename(argv[3])))
    {
        printf("Nombre de archivo incorrecto\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

}

void VerificarArgumentosINET6(char *argv[])
{
    // Verificar que la dir IPV6 ingresada sea correcta
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(!isValidIPV6Address(argv[4]))
    {
        printf("Debe ingresar una dir IPV4 correcta\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que el puerto ingresado para IPV4 sea un este compuesto de digitos y tenga un valor correcto
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int i = 0; i < strlen(argv[5]); i ++)
    {
        if((isdigit(argv[5][i]) == 0)|| (atoi(argv[5]) <= 0) || (atoi(argv[5]) > 65535)) // Verificar que en el argumento para el puerto se haya
        {                                                                // ingresado un num y no letras ni caracteres especiales
            printf("Debe ingresar un puerto correcto\n");                // y que este num sea > = 0 y < 65535
            exit(EXIT_FAILURE);
        }
    }                                           // El valor se escribe en iport LUEGO de verrificar que el valor sea correcto ya el valor ingresado 
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Verificar que la interfaz ingresada solo consiste en letras y digitos. Si pasa esta prueba, de todas formas tiene que ser una interfaz
    // correcta, de otra manera va a fallar bind()
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(unsigned int i = 0; i < strlen(argv[6]); i ++)
    {
        if((isdigit((argv[6][i]) == 0) && (isalpha(argv[6][i]) == 0)) || strlen(argv[6])>MAXLINE) 
        {                                                               
            printf("Debe ingresar una interfaz correcta\n");               
            exit(EXIT_FAILURE);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
}
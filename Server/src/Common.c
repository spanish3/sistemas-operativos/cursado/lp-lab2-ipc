#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // write, close, etc
#include <ctype.h> // isdigit()
#include <arpa/inet.h> // inet_pton()
#include <sys/un.h> // direciiones unix
#include <net/if.h> // INET6
#include <netinet/in.h> // INET6
#include <pthread.h>
#include <errno.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <sqlite3.h>
#include <sys/stat.h> // stat para tam de archivo
#include <fcntl.h> // open() para archivos

#include "Arg-Verify-Server.h"
#include "ConectionPoolThread.h"
#include "ConectionThread.h"
#include "Common.h"

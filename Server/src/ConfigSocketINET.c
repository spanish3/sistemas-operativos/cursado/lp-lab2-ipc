#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"

void ServerConfigSocketINET(int *sock, struct sockaddr_in *servaddr, int iport, long unsigned int max, char *stringaddr)
{
    // Crear Socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((*sock = socket(AF_INET, SOCK_STREAM,0)) < 0)
    {
        printf("Error al crear el socket - servidor\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Que se pueda reutilizar
    //--------------------------------------------------------------------------------------------------------------------------------------------
    int yes=1;

    if (setsockopt(*sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    //CurrentAHAmountr el tipo, puerto y address del socker
    //--------------------------------------------------------------------------------------------------------------------------------------------
    bzero(servaddr, sizeof(*servaddr)); //Limpiar estructura servaddr
    servaddr->sin_family = AF_INET; // Es un socket para comunicarse con procesos remotos mediante IPV4
    servaddr->sin_addr.s_addr = inet_addr(stringaddr); // Obtener ip de los argumentos
    servaddr->sin_port = htons((unsigned short int)iport); // Obtener puerto de los argumentos
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Asociar el socket a la dirección especificada
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((bind(*sock, (SA *) servaddr, sizeof(*servaddr))) < 0)
    {
        printf("Error al asociar el socket con la dir INET\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Escuchar en el socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((listen(*sock, (int)max)) < 0) 
    {
        printf("Error en escucha de mensaje\n");
        exit(EXIT_FAILURE);
    }
    // listen(int sockfd, int blacklog) establece que el socket indicado por el fd sockfd es
    // pasivo, es decir espera conexiones entrantes para aceptarlas mediante la llamada accept()
    // El argumento backlog se refiere al num max de conexiones pendientes (hay una cola de estas)
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

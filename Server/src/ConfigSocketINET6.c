#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"

void ServerConfigSocketINET6(int *sock, struct sockaddr_in6 *servaddr, int iport, long unsigned int max, char *stringaddr, char *interfaceName)
{
    // Crear Socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((*(sock) = socket(AF_INET6, SOCK_STREAM,0)) < 0)
    {
        printf("Error al crear el socket - servidor\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    int yes=1;

    if (setsockopt(*(sock), SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    // Llenar datos para espesificar el tipo, puerto y address del socker
    //--------------------------------------------------------------------------------------------------------------------------------------------
    bzero(servaddr, sizeof(*servaddr)); //Limpiar estructura servaddr, esot es lo mismo que memset(&servaddr,0,sizeof(servaddr));
    servaddr->sin6_family = AF_INET6; // Es un socket para comunicarse con procesos remotos mediante IPV4
    if(inet_pton(AF_INET6, stringaddr, &(servaddr->sin6_addr)) != 1)
    {
        printf("Dir mal\n");
        exit(EXIT_FAILURE);
    }
    servaddr->sin6_port = htons((unsigned short int)iport); // Obtener puerto de los argumentos
    servaddr->sin6_scope_id = if_nametoindex(interfaceName); // enp2s0
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Asociar el socket a la dirección espesificada
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((bind(*(sock), (SA *) servaddr, sizeof(*servaddr))) < 0)
    {
        printf("Error al asociar el socket con la dir INET6\n");
        printf("El errno es %s\n",errnoname((int)errno));
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Escuchar en el socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((listen(*(sock), (int)max)) < 0) 
    {
        printf("Error en escucha de mensaje\n");
        exit(EXIT_FAILURE);
    }
    // listen(int sockfd, int blacklog) establece que el socket indicado por el fd sockfd es
    // pasivo, es decir espera conexiones entrantes para aceptarlas mediante la llamada accept()
    // El argumento backlog se refiere al num max de conexiones pendientes (hay una cola de estas)
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

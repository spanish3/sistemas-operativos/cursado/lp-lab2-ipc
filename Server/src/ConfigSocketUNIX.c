#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"

void ServerConfigSocketUNIX(int *sock, struct sockaddr_un *servaddr, long unsigned int max, char *filename)
{    
    // Crear Socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((*(sock) = socket(AF_UNIX, SOCK_STREAM,0)) < 0)
    {
        printf("Error al crear el socket - servidor\n");
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    unlink (filename);

    // Handlear el address (path del archivo) del server con el que se quiere conectar
    //--------------------------------------------------------------------------------------------------------------------------------------------
    bzero(servaddr, sizeof(*servaddr)); //Limpiar estructura servaddr
	servaddr->sun_family = AF_UNIX;
    //strcpy(file_path,filename);
    //mkdir(file_path, 0700);
    //strcpy(file_name,file_path);
    //strcat(file_name,"UNIX_FILE");
    strcpy( servaddr->sun_path, filename);
    //--------------------------------------------------------------------------------------------------------------------------------------------

    int yes=1;

    if (setsockopt(*(sock), SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    // Asociar el socket a la dirección espesificada
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((bind(*(sock), (SA *) servaddr, sizeof(*servaddr))) < 0)
    {
        printf("Error al asociar el socket con la dir UNIX\n");
        printf("El errno es %s\n",errnoname((int)errno));
        exit(EXIT_FAILURE);
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Escuchar en el socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if((listen(*(sock), (int)max)) < 0) 
    {
        printf("Error en escucha de mensaje\n");
        exit(EXIT_FAILURE);
    }
    // listen(int sockfd, int blacklog) establece que el socket indicado por el fd sockfd es
    // pasivo, es decir espera conexiones entrantes para aceptarlas mediante la llamada accept()
    // El argumento backlog se refiere al num max de conexiones pendientes (hay una cola de estas)
    //--------------------------------------------------------------------------------------------------------------------------------------------
}

#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"
#include "ConfigSocketUNIX.h"
#include "TaskHandlingThread.h"
#include "CountingThread.h"

void *UNIX_Server_Code(void *arg)
{
    struct UNIX_arg_struct *arguments = arg; // Recuperar argumentos
    
    // Variables: Crear / configurar sockets
    //--------------------------------------------------------------------------------------------------------------------------------------------
    int listenfd; // File descriptor para el socket que queda a escucha de conexiones.
    int *connfd; // File descriptor para el socket que proviene de accept() (conection established)
    struct sockaddr_un servaddr; // Estructura para espesificar el server address

    connfd = malloc((unsigned long int)arguments->maxClientes * sizeof(int));
    for(int i = 0; i < arguments->maxClientes; i++)
    {
        connfd[i] = -1;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Variables: Crear hilos para handlear las conexiones | Asignar memoria: Tantos TaskThreads, CountingThreads y argumentos como maxClientes
    //--------------------------------------------------------------------------------------------------------------------------------------------
    pthread_t *TaskThread;
    pthread_t *CountingThread;
    struct local_threads_arg_struct *Handler_Thread_Args;
    pthread_mutexattr_t mta; // para configurar atributos de los locks
    pthread_mutex_t *handlers_lock;
    pthread_mutex_t lock;

    pthread_mutexattr_init(&mta); // Inicializar mta con los valores por defecto
    pthread_mutexattr_setrobust(&mta,PTHREAD_MUTEX_ROBUST); // Robusto: Si un hilo toma el lock y muere antes de liberarlo, se libera auto.
    pthread_mutexattr_settype(&mta,PTHREAD_MUTEX_RECURSIVE); // Recursivo: Esto soluciona el error en la syscall cuando se ejecuta lock() muy seguido
    
    pthread_mutex_init(&lock,&mta); 

    TaskThread = malloc((unsigned long int)arguments->maxClientes * sizeof(pthread_t)); 
    CountingThread = malloc((unsigned long int)arguments->maxClientes * sizeof(pthread_t)); 
    Handler_Thread_Args = malloc((unsigned long int)arguments->maxClientes * sizeof(struct local_threads_arg_struct));
    handlers_lock = malloc((unsigned long int)arguments->maxClientes * sizeof(pthread_mutex_t));

    for(int i = 0; i <arguments->maxClientes; i++)
    {
        pthread_mutex_init(&handlers_lock[i],&mta); 
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // Variables: Limitar la cantidad de hilos handlers que se pueden lanzar y determinar el primero disponible. Asignar memoria
    //--------------------------------------------------------------------------------------------------------------------------------------------
    long unsigned int CurrentAHAmount = 0; // Cantidad de handlers disponibles en determinado momento
    int *AvailableHandlers; // Array de int que sirve como banderas, si AvailableHandlers[i] == 1 -> el handler i esta dispponible
    int nextHandler = 0; // Este int se valua con el primer handler que se encuentre disponible en determinado momento

    AvailableHandlers = malloc((unsigned long int)arguments->maxClientes * sizeof(int)); // Asginar memoria: El array tendra tantos elementos como maxClientes
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Configurar socket
    //--------------------------------------------------------------------------------------------------------------------------------------------
    ServerConfigSocketUNIX(&listenfd, &servaddr, (unsigned long int)arguments->maxClientes, arguments->UNIX_File_Name);
    //--------------------------------------------------------------------------------------------------------------------------------------------
     
    // Inicialmente todos los handlers estan disponibles, llenar el array de indicadores con "1" -> "Disponible"
    //--------------------------------------------------------------------------------------------------------------------------------------------
    for(int i = 0; i < arguments->maxClientes; i++)
    {
        AvailableHandlers[i] = 1;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    // El hilo para UNIX se encuentra siempre esperando conexiones y lanzando hilos para que se encarguen de estas cuando se establecen
    //--------------------------------------------------------------------------------------------------------------------------------------------
    while(*(arguments->salir) == 0) // Si se ingresa "salir" en main, ya no se esperan conexiones
    {   
        // Obtener cantidad de handlers disponibles. Si ya hay tantos hilos trabajando como maxClientes, se debe espearar hasta que uno termine
        //----------------------------------------------------------------------------------------------------------------------------------------// Se accede en ex mutua, pues los hilos handlers modifican AvailableHandlers liberando un handler al terminar
        CurrentAHAmount = (long unsigned int)getAvailableHandlersAmount(AvailableHandlers,(unsigned long int)arguments->maxClientes,handlers_lock);

        if(CurrentAHAmount == 0 || getFirstAvailableHandler(AvailableHandlers,(unsigned long int)arguments->maxClientes,handlers_lock) < 0)
        {
            //printf("No hay Handlers UNIX disponibles, espere hasta que uno se desocupe\n");
            while(CurrentAHAmount == 0)
            {
                if(*(arguments->salir) == 0) 
                {
                    CurrentAHAmount = (long unsigned int)getAvailableHandlersAmount(AvailableHandlers,(unsigned long int)arguments->maxClientes,handlers_lock);
                }
                else
                {
                    break;
                }
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------------
        
        // Sabiendo que hay handlers disponibles, obtener el primero disponible en orden numerico
        //----------------------------------------------------------------------------------------------------------------------------------------
        if(*(arguments->salir) == 0) 
        {
            nextHandler = getFirstAvailableHandler(AvailableHandlers,(unsigned long int)arguments->maxClientes,handlers_lock);
        }
        CurrentAHAmount = 0;
        //----------------------------------------------------------------------------------------------------------------------------------------

        // Espera una conexion y handlear el error en caso de que ocurra. Se utilizara el fd conn[nextHandler] -> primer handler disponible
        //----------------------------------------------------------------------------------------------------------------------------------------
        int flags = fcntl(listenfd, F_GETFL, 0);       // Configurar para que accept() pase a ser
        fcntl(listenfd, F_SETFL, flags | O_NONBLOCK);  // no bloqueante.
        while(connfd[nextHandler] < 0)
        {
            if(*(arguments->salir) == 0)
            {
                connfd[nextHandler] = accept(listenfd, (SA *) NULL, NULL); // NULL -> No importa quien se conecte, aceptar la conexión
                /*if(connfd[nextHandler] == -1) // Ya no chequeo el error poque mi accept es no bloqueante y espero en este while
                {                               // hasta que se establezca una conexion correcta
                    printf("Error en accept()\n");
                    exit(EXIT_FAILURE);
                }*/
            }
            else
            {
                break;
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------------
        
        
        // Ahora que hay conexion, se llenan los argumentos, se lanza el hilo handler de esta conexion y se modifica AvalableHandlers en ex mutua
        //----------------------------------------------------------------------------------------------------------------------------------------
        //printf("Conexion establecida. Manejada por el hilo %d\n",nextHandler); 
        if(*(arguments->salir) == 0)
        {
            Handler_Thread_Args[nextHandler].id = nextHandler;
            Handler_Thread_Args[nextHandler].checksegs = 0;
            Handler_Thread_Args[nextHandler].ConnSocket = &(connfd[nextHandler]);
            Handler_Thread_Args[nextHandler].ExitThread = 0;
            Handler_Thread_Args[nextHandler].Handlers = AvailableHandlers;
            Handler_Thread_Args[nextHandler].handlers_lock = &handlers_lock[nextHandler];
            Handler_Thread_Args[nextHandler].lock = &lock;
            Handler_Thread_Args[nextHandler].req_list_lock = arguments->req_list_lock;
            Handler_Thread_Args[nextHandler].list = arguments->list;
            Handler_Thread_Args[nextHandler].salir = arguments->salir;
            
            ocuparHandler(AvailableHandlers,nextHandler,&handlers_lock[nextHandler]);
            
            pthread_create(&(TaskThread[nextHandler]),NULL,Task,&(Handler_Thread_Args[nextHandler]));
            pthread_create(&(CountingThread[nextHandler]),NULL,ThreadCode,&(Handler_Thread_Args[nextHandler]));

        }
        
        //----------------------------------------------------------------------------------------------------------------------------------------
    }

    // Si se desea salir del servidor, esperar a todos los hilos lanzados por este hilo INET (estos hilos terminan si salir == 1)
    //--------------------------------------------------------------------------------------------------------------------------------------------
    if(*(arguments->salir) == 1)
    {
        for(int i = 0; i < arguments->maxClientes; i++)
        {
            pthread_join(TaskThread[i],NULL);
            pthread_join(CountingThread[i],NULL);
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------

    // Luego cerrar los fd, verificar errores y liberar toda la memoria asignada
    //--------------------------------------------------------------------------------------------------------------------------------------------
          
    if((close(listenfd) < 0)) // Cerrar fd de escucha de conexiones
    {
        printf("Error all cerrar listenfd\n");
        exit(EXIT_FAILURE); 
    }

    for(int i = 0; i < arguments->maxClientes; i++)
    {
        pthread_mutex_destroy(&handlers_lock[i]);
    }
    pthread_mutex_destroy(&lock);

    free(AvailableHandlers);   // Liberar array indicador de handlers disponibles
    free(TaskThread);          // Liberar array de hilos para manejar conexiones 
    free(CountingThread);      // Liberar array de hilos para cerrar hilos handlers mediante timeout 
    free(connfd);              // Liberar array de fd para conexiones
    free(handlers_lock);
    free(Handler_Thread_Args); // Liberar array argumentos para hilos handlers
    //--------------------------------------------------------------------------------------------------------------------------------------------

    return NULL;
}
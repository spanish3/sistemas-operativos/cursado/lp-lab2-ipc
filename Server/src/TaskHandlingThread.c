#include "Common.h"
#include "CommonFunctions.h"
#include "DataStructure.h"

void* Task(void * arg)
{
    struct local_threads_arg_struct *arguments = arg;
    
    char recvline[MAXLINE+1]; // Buffer para recibir. Es MAXLINE + 1 porque...
    char aux[MAXLINE];         // Auxiliar para quitar el checksum y el end of messege
    long int readBytes;

    // Variables para esperar a que la conexion que esta siendo utilizada reciba el ack
    //--------------------------------------------------------------------------------------------------------------------------------------------
    int waitACK = 1;
    int conNumber;
    //--------------------------------------------------------------------------------------------------------------------------------------------
    
    //printf("soy task %d con socketcon %d\n",arguments->id,*(arguments->ConnSocket));

    while((arguments->ExitThread == 0) && (*(arguments->salir) == 0)) 
    {
        // Leer mensaje: Inicialmente readBytes es cero, siempre y cuando no se desee salir, se llama a recv. Si readBytes > 0 -> se ha 
        // recibido un dato.
        //----------------------------------------------------------------------------------------------------------------------------------------
        if((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
        {
            readBytes = 0;
            while(readBytes <= 0)
            {
                if((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
                {
                    readBytes = recv(*(arguments->ConnSocket), recvline, MAXLINE-1, MSG_DONTWAIT);
                }
                //if(arguments->ExitThread) // Si se acaba el tiempo o se produce un error de lectura (que puede ser pot timeout), salir
                else
                {
                    /*if(readBytes < 0) // Ya no handleo el error porque utilizo recv no bloquenate y espero hasta que haya un resultado correcto
                    {
                        printf("Error de lectura en task %d\n",arguments->id);
                        printf("El errno es %s\n",errnoname((int)errno));
                    }*/
                    if(*(arguments->ConnSocket) != -1)
                    {
                        if(close(*(arguments->ConnSocket)) < 0)
                        {
                            printf("Soy task %d, Error al cerrar la conec %d\n",arguments->id,*(arguments->ConnSocket));
                            printf("El errno es %s\n",errnoname((int)errno));
                        }
                        else
                        {
                            //printf("Soy task %d, cierro el fd %d\n",arguments->id,*(arguments->ConnSocket));
                            *(arguments->ConnSocket) = -1;
                        }
                    }
                    //*(arguments->ConnSocket) = -1;
                    break;
                }
            }
        }
        //----------------------------------------------------------------------------------------------------------------------------------------

        //
        //----------------------------------------------------------------------------------------------------------------------------------------
        if(!arguments->ExitThread && *(arguments->salir) == 0)
        {
            if(readBytes > 0)
            {
                // Si se han recibido datos, modificar checksegs para resetear el timer que cierra este hilo
                //--------------------------------------------------------------------------------------------------------------------------------
                if((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
                {
                    pthread_mutex_lock(arguments->lock);
                    arguments->checksegs = 0;
                    pthread_mutex_unlock(arguments->lock);
                }
                //--------------------------------------------------------------------------------------------------------------------------------
                
                // Detectar final del dato recibido (el final de los mensajes http suele tener \n\r\n\r, al detectar un \n podemos intuir -> fin
                //--------------------------------------------------------------------------------------------------------------------------------
                if(recvline[readBytes - 1] == '\n')   
                {                                   
                    // Obtener el tipo de cliente del que proviene el mensaje
                    //----------------------------------------------------------------------------------------------------------------------------
                    char pureMsg[MAXLINE];
                    char t;
                    if((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
                    {
                        t = getTypeAndMessege(recvline,pureMsg);
                    }
                    //----------------------------------------------------------------------------------------------------------------------------
                    
                    // Si el cliente es tipo C se debe agregar a la cola de requests el mensaje pero NO el ack del tam para el handshake: 
                    // tam-> ; <-ack ; -> archivo
                    //----------------------------------------------------------------------------------------------------------------------------
                    if(t == 'c')
                    {
                        //printf("Es tipo C\n");
                        // Verificar que el mensaje no sea un ack (Tam recibido) ni tampoco ""
                        //------------------------------------------------------------------------------------------------------------------------
                        if((strcmp(recvline,"Tam recibido\n")) && (recvline[0] != '\0')) // si no es un ack, agregarlo a la lista
                        {
                            strcpy(aux,recvline);
                            pthread_mutex_lock(arguments->req_list_lock);
                            add_db_request(arguments->list, db_request_list_getNextID(arguments->list),arguments->ConnSocket,aux);
                            pthread_mutex_unlock(arguments->req_list_lock);
                            // Luego de agregar el mensaje se debe esperar hasta que el hilo de conexion reciba el ack, para eso se usa el array
                            // llamado waitACK (inicialmente waitACK[i] es 1 y al recibir el ack el hilo i, waitACK[i] se hace cero). 
                            
                            // Primero se determina que elemento i de waitACK[] corresponde con el hilo que se encuentra trabajando con esta
                            // conexion (mediante el num de socket de la conexion con el cliente)
                            //--------------------------------------------------------------------------------------------------------------------
                            for(int i = 0; i < 5; i++)
                            {
                                if(*(arguments->ack_arg[i].ConnSocket) == *(arguments->ConnSocket))
                                {
                                    conNumber = i;
                                }
                            }
                            //--------------------------------------------------------------------------------------------------------------------
                            
                            // Esperar hasta que el conectionThread reciba el ack, si este hilo no espera, el ack va a ser extraido por este hilo
                            //--------------------------------------------------------------------------------------------------------------------
                            while(waitACK) 
                            {
                                if((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
                                {
                                    pthread_mutex_lock(arguments->ack_arg[conNumber].ack_lock);
                                    waitACK = *(arguments->ack_arg[conNumber].ack);
                                    pthread_mutex_unlock(arguments->ack_arg[conNumber].ack_lock);
                                }
                                else
                                {
                                    break;
                                }
                            }
                            if(!waitACK)
                            {
                                //printf("Task %d; Recibido ack\n",arguments->id);
                                // ack = 1 entonces, en el cliente tipo C, conectionThread recibe un mensaje del cliente en lugar de taskHandlingThread (que espera a conectionThread)
                                pthread_mutex_lock(arguments->ack_arg[conNumber].ack_lock);
                                *(arguments->ack_arg[conNumber].ack) = 1;
                                pthread_mutex_unlock(arguments->ack_arg[conNumber].ack_lock);
                            }
                            //--------------------------------------------------------------------------------------------------------------------
                        }
                        //------------------------------------------------------------------------------------------------------------------------
                        //liberarHandler(arguments->Handlers,arguments->id,arguments->lock);
                        pthread_mutex_lock(arguments->lock);
                        arguments->ExitThread = 1;
                        pthread_mutex_unlock(arguments->lock);
                        //printf("Task %d cambia ExitTHread\n",arguments->id);
                    }
                    //----------------------------------------------------------------------------------------------------------------------------
                    // Si es un cliente tipo A o B simplemente se agrega el mensaje a la cola de requests
                    if(t == 'b')
                    {
                      //printf("Es tipo B\n");
                      if((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
                        {
                            strcpy(aux,recvline);
                            if(!strcmp(aux,"Tipo B | salir\n"))
                            {
                                pthread_mutex_lock(arguments->lock);
                                arguments->ExitThread = 1;
                                pthread_mutex_unlock(arguments->lock); 
                                //printf("Hay que salir task\n");
                            }
                            pthread_mutex_lock(arguments->req_list_lock);
                            add_db_request(arguments->list, db_request_list_getNextID(arguments->list),arguments->ConnSocket,aux);
                            pthread_mutex_unlock(arguments->req_list_lock);
                        }
                    }
                    if(t == 'a')
                    {
                        //printf("Es tipo A\n");
                        if((arguments->ExitThread == 0) && (*(arguments->salir) == 0))
                        {
                            strcpy(aux,recvline);
                            pthread_mutex_lock(arguments->req_list_lock);
                            add_db_request(arguments->list, db_request_list_getNextID(arguments->list),arguments->ConnSocket,aux);
                            pthread_mutex_unlock(arguments->req_list_lock);
                        }
                        pthread_mutex_lock(arguments->lock);
                        arguments->ExitThread = 1;
                        pthread_mutex_unlock(arguments->lock);
                    }
                    //----------------------------------------------------------------------------------------------------------------------------
                    
                    // Limpiar readBytes y recvline para que no queden datos de mensajes anteriores
                    //----------------------------------------------------------------------------------------------------------------------------
                    readBytes = 0;
                    memset(recvline, 0, MAXLINE); 
                    //----------------------------------------------------------------------------------------------------------------------------
                }
                //--------------------------------------------------------------------------------------------------------------------------------
            }
        }
        else
        {
            break;
        }
        
    }

    while(*(arguments->ConnSocket) != -1){}

    liberarHandler(arguments->Handlers,arguments->id,arguments->handlers_lock);

    return NULL;
}